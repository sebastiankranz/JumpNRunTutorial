﻿using System;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float Gravity = 1.5f;
    public float LaunchVelocity = 0.25f;
    public float GlideVelocity = 1.1f;
    public float WalkingSpeed = 10f;
    public float JumpDuration = 0.5f;
    public float TurnSpeed = 10f;

    Animator anim;
    CharacterController controller;

    private float _verticalVelocity;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
    }


    // Update is calaed once per frame
    void Update()
    {
        // Apply Gravity
        _verticalVelocity -= Gravity * Time.deltaTime;

        // Start Jump
        if (
            Input.GetButtonDown("Jump")
            && controller.isGrounded
        )
        {
            StartCoroutine(Jump());
        }

        // Walk
        var horizontalVelocity = Input.GetAxis("Horizontal")
                                 * WalkingSpeed
                                 * Time.deltaTime;

        // Apply Movement
        controller.Move(new Vector3(horizontalVelocity, _verticalVelocity, 0));
        if (controller.isGrounded) _verticalVelocity = Math.Max(_verticalVelocity, 0);

        // Update Variables for Animator
        anim.SetFloat("HorizontalSpeed", Math.Abs(horizontalVelocity / (WalkingSpeed * Time.deltaTime)));
        anim.SetBool("IsGrounded", controller.isGrounded);

        // Update Rotation
        Vector3 targetDirection = transform.forward;
        if (horizontalVelocity > 0) targetDirection = Vector3.right;
        if (horizontalVelocity < 0) targetDirection = Vector3.left;
        var targetRotation = Quaternion.LookRotation(targetDirection);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * TurnSpeed);
    }


    private IEnumerator Jump()
    {
        var jumpTime = 0f;
        _verticalVelocity += LaunchVelocity;
        while (Input.GetButton("Jump") && jumpTime < JumpDuration)
        {
            _verticalVelocity += GlideVelocity * Time.deltaTime;
            jumpTime += Time.deltaTime;
            yield return null;
        }
    }
}

